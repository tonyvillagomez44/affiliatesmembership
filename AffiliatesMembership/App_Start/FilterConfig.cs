﻿using System.Web;
using System.Web.Mvc;
using AffiliatesMembership.Infraestructure;

namespace AffiliatesMembership
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new TransactionFilter());
            filters.Add(new HandleErrorAttribute());
        }
    }
}
