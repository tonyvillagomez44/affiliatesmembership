﻿using AffiliatesMembership.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AffiliatesMembership.Areas.Admin.Controllers
{
    public class AuthController : Controller
    {
        // GET: Admin/Auth
        [Authorize(Roles = "Admin")]
        public ActionResult Login()
        {
            return View();
        }
    }
}